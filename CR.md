# TP GPU AC4
Léopold Clément

## Introduction
Pour le traitement des calculs parallèle, l'utilisation d'accelérateur matériel est répendu.
La technologie d'accélération la plus commune est le GPGPU.
Nous allons étudier un système embarqué Nvidia, le leader du marché du GPGPU.
Le language utiliser pour la programation de cette platforme est Cuda, un language dédié.

## TP0 - Multiplication de matrice

Voici un tableau récapitulatif du temps de calcul de multiplication de matrice de différentes tailles, selon la méthode de calcul.

Taille | CPU | CUDA_v0 | cuda_v0_bis | CUDA_v1 | CUDA_CUBLAS
-------+-----+---------+-------------+---------+------------
128 | 44 | 10 (60%) | 1 (79%) | 1 (85%) | 25 (2%)
256 | 293 | 13 (12%) | 4 (38%) | 3 (58%) | 1 (72%)
512 | 2389 | 150 (4%) | 26 (20%) | 14 (35%) | 8 (82%)
1024 | 34605 | 771 (2%) | 258 (10%) | 165 (11%) | 67 (85%)

Le calcul sur CPU est la méthode "classique".

// TODO

## TP1 - Changement de couleur

Pour ilustrée la rédaction d'un code Cuda, on va commencer par une opération simple : changer la couleur d'une voiture.
Notre but est de prendre une voiture rouge et de la rendre jaune.

``` cuda
__global__ void seuillage_kernel(float d_image_out[][SIZE_J][SIZE_I],float d_image_in[][SIZE_J][SIZE_I])
{
	int i = blockIdx.x*BLOCK_SIZE + threadIdx.x;
	int j = blockIdx.y*BLOCK_SIZE + threadIdx.y;
	
	float eta = d_image_in[0][j][i] / powf(powf(d_image_in[0][j][i],2) + powf(d_image_in[1][j][i],2) + powf(d_image_in[2][j][i],2) , 0.5);
	
	d_image_out[0][j][i] = d_image_in[0][j][i];
	d_image_out[1][j][i] = d_image_in[1-(eta > 0.7)][j][i];
	d_image_out[2][j][i] = d_image_in[2][j][i];
}
```

Le code du kernel Cuda décris le traitement réaliser par un thread.
On commence par calculer ```i``` et ```j``` les coordonnée du pixel que le thread va traiter.
On a choisit d'avoir un thread par pixel.

Le resultat est // TODO

## TP2 - Réduction

On veux maintenant réaliser une réduction de sommation, càd sommer tous les élements d'un vecteur.
Pour cela, on commence par demander à chaque thread de sommée plusieurs valeurs du vecteur.
Puis chaque block va sommé toutes les valeurs accumulés par ses threads.
Pour cela, on utilise la mémoire partagé des blocks pour faire communiqué les threads d'un même blocks.
Il faut faire attention à la synchronisation des threads, car il ne sont pas tous éxecuter simultanément, mais pas warp de 32 threads.

Une fois l'accumulation réaliser dans chaque block, l'accumulation final est réaliser par le CPU pour simplifier le code, car sinon il faudrais faire communiquer les blocks entre eux, ce qui n'est pas aisé.

